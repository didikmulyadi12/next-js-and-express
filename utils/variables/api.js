export default class Api {

    static baseUrl  =   "/";
    static baseAsset=   "/";

    static header = function(token){
        return {
            headers : {
                'Authorization' : token
            }
        }
    }
}