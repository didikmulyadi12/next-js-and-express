export default class stringHelper {
    static encodeURIComponent = function(url = ""){
        if(url.length > 3){
            let url_array = url.split("/");
            if(url_array.length > 2){
                return url_array[0]+'/'+url_array[1]+'/'+url_array[2]+'/'+encodeURIComponent(url_array[3]);    
            }
        }
    }

    static formatRupiah =   function(angka){
        if(typeof angka == 'undefined') {
            return 0;
        }else{
            var angka           = angka.toString();
            var number_string   = angka.replace(/[^,\d]/g, '').toString();
            var split   		= number_string.split(',');
            var sisa     		= split[0].length % 3;
            var rupiah     		= split[0].substr(0, sisa);
            var ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
        
            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                var separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
        
            return rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        }       
    }

    static concatWithZero(number){
        if(parseInt(number) < 10){
            return "0"+number
        }else{
            return number
        }
    }

    static checkFileSize(size) {
        const maxSize = 5120000;
        if (maxSize >= size) {
            return true;
        } else {
            return false;
        }
    }

    static checkImageFile(type) {
        var type = type.toLowerCase();
        if ((type.match(/jpeg/g)) || (type.match(/jpg/g)) || (type.match(/png/g))) {
            return true;
        }else {
            return false;
        }
    }

    static checkDocumentFile(type) {
        var type = type.toLowerCase();
        if ((type.match(/pdf/g)) || (type.match(/doc/g)) || (type.match(/xlx/g))) {
            return true;
        }else {
            return false;
        }
    }

    static ucapanPagiSiangSore() {
        let date = new Date();
        let hours= date.getHours();

        if (hours < 12){
            return "Good Morning";
        }else if(hours >= 12 && hours <= 16){
            return "Good Afternoon";
        }else{
            return "Good Evening";
        }
    }

    
}