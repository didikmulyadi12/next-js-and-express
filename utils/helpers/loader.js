import React from 'react'
import swal from 'sweetalert'

export const callLoader = () => {
    if(!(swal.getState().isOpen)){
        swal({
            icon : '/static/animated-icons/loader.gif',
            className : 'loader-custom',
            buttons : false,
            closeOnClickOutside : false,
            closeOnEsc : false
        })
    } 
    
}

export const stopLoader = () => {
    swal.getState().isOpen ? swal.close() : ""
}