import StringHelper from './stringHelper'

export default class DateHelper {

    static convertDateToHourMinute(created_at){ // 18:30
        if(created_at != ""){
            var date = new Date(created_at.toString());
            return StringHelper.concatWithZero(date.getHours())+':'+StringHelper.concatWithZero(date.getMinutes());
        }else{
            return ""
        }
        
    }

    static convertDateToHourMinuteSecond(created_at){ // 18:30:11
        if(created_at != ""){
            var date = new Date(created_at.toString());
            return StringHelper.concatWithZero(date.getHours())+':'+StringHelper.concatWithZero(date.getMinutes())+':'+StringHelper.concatWithZero(date.getSeconds());
        }else{
            return ""
        }
        
    }

    static  convertDateToShortMonth(created_at){ // 12 Mei 2019
        if(created_at != ""){
            var monthArray = ["Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agu","Sept","Okt","Nov","Des"]
            var date = new Date(created_at.toString());
            return date.getDate()+' '+(monthArray[date.getMonth()])+' '+date.getFullYear();
        }else{
            return ""
        }
    }

    static  covertDateToShortMonthAndShortYear(created_at){ // 12 Mei 19
        if(created_at != ""){
            var monthArray = ["Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agu","Sept","Okt","Nov","Des"]
            var date = new Date(created_at.toString());
            return StringHelper.concatWithZero(date.getDate())+' '+(monthArray[date.getMonth()])+' '+date.getFullYear().toString().substr(2,2);
        }else{
            return ""
        }
    }

    static  convertDateToShortDate(created_at){ // 12/04/2019
        if(created_at != ""){
            var monthArray = ["01","02","03","04","05","06","07","08","09","10","11","12"]
            var date = new Date(created_at.toString());
            return StringHelper.concatWithZero(date.getDate())+'/'+(monthArray[date.getMonth()])+'/'+date.getFullYear();
        }else{

        }
        
    }

    static  convertDateToShortDateWithDay(created_at){ // Minggu, 01/01/2019
        if(created_at != ""){
            var monthArray = ["01","02","03","04","05","06","07","08","09","10","11","12"]
            var dayArray = ["Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu"];
            var date = new Date(created_at.toString());
            return dayArray[date.getDay()]+', '+StringHelper.concatWithZero(date.getDate())+'/'+(monthArray[date.getMonth()])+'/'+date.getFullYear();    
        }else{
            return ""
        }
        
    }

    static  convertDateToShortDateWithTime(created_at){ // 9/10/2019 10:10
        if(created_at != ""){
            var date = new Date(created_at.toString());
            return date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear()+' '+date.getHours()+':'+(date.getMinutes())
        }else{
            return ""
        }
        
    }

    static konversiDateDMY(created_at){ // 10-01-2019
        if(created_at != ""){
            var monthArray = ["01","02","03","04","05","06","07","08","09","10","11","12"]
            var date = new Date(created_at.toString());
            return StringHelper.concatWithZero(date.getDate())+'-'+ (monthArray[date.getMonth()])+'-'+date.getFullYear();
        }else{
            return ""
        }
    }

    static konversiDateYMD(created_at){ // 2019-10-10
        if(created_at != ""){
            var monthArray = ["01","02","03","04","05","06","07","08","09","10","11","12"]
            var date = new Date(created_at.toString());
            return date.getFullYear()+'-'+(monthArray[date.getMonth()])+'-'+StringHelper.concatWithZero(date.getDate());
        }else{
            return "";
        }
        
    }
}