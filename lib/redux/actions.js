import {
    SET_CLIENT_STATE
} from './names'

export const setClientState = (state) => ({
    type: SET_CLIENT_STATE,
    payload: state
});
